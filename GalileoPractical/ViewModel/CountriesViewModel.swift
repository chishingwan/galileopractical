//
//  CountriesViewModel.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

class CountriesViewModel{

    public var countryName = BehaviorRelay<String>(value: defaultValue.empty.rawValue)
    public var flag = BehaviorRelay<String>(value: defaultValue.empty.rawValue)
    public var capital = BehaviorRelay<String>(value: defaultValue.empty.rawValue)
    public var alphaCode = BehaviorRelay<String>(value: defaultValue.empty.rawValue)
    public var population = BehaviorRelay<String>(value: defaultValue.empty.rawValue)
    
    public var countryList = BehaviorRelay<[Country]>(value: [Country]())
    public var countryInfo = BehaviorRelay<Country?>(value: Country(map: emptyMap))
    
    
    func fetchCountryData(){
        Singleton.fetchCountryData(completion: {error in
            if let countries = Singleton.countries {
                self.countryList.accept(countries)
            } else if (error != nil){
                print("connection error")
            }
        })
    }
    
    func getCountryList(index: Int){
//        countryList.accept([countryInfo.value!])
        countryList.accept(countryList.value)
    }
    
    func setUpCountryDetails(index: Int){
        getCountryList(index: Singleton.selectedCountry)
        let countryDetails = countryList.value[index]
        
        countryName.accept(countryDetails.countryName.getOrDefault(defaultValue.empty.rawValue, isEmptyIncluded: true))
        flag.accept(countryDetails.flag.getOrDefault(defaultValue.empty.rawValue, isEmptyIncluded: true))
        capital.accept(countryDetails.capital.getOrDefault(defaultValue.empty.rawValue, isEmptyIncluded: true))
        alphaCode.accept(countryDetails.alpha2Code.getOrDefault(defaultValue.empty.rawValue, isEmptyIncluded: true))
        population.accept(countryDetails.population.getOrDefault(defaultValue.empty.rawValue, isEmptyIncluded: true))
    }
}
