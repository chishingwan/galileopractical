//
//  CountryCells.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/14/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import Foundation
import UIKit


class CountryCells: UITableViewCell {
    
    @IBOutlet var cName: UILabel!
    @IBOutlet var flag: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
