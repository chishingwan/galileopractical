//
//  APIManager.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import ObjectMapper

class APIManager{
    
    func getCountryData(completion: @escaping (String?,AFError?)->Void) {
        apiRequest(APIDefinitions.urlBase, completion: { data, error in completion(data, error)})
    }
}

extension APIManager{
    
    func apiRequest(_ url: String,
                    parameter: Parameters? = nil,
                    method: HTTPMethod = .get,
                    encoding: ParameterEncoding = JSONEncoding.default,
                    headers: HTTPHeaders? = nil,
                    completion: @escaping (String?,AFError?)->Void){
        AF.request(url, method: method, parameters: parameter, encoding: encoding, headers: headers)
        .validate(statusCode: 200..<300)
        .responseString(completionHandler: {response in
            switch response.result {
                case .success(let data):
                    completion(data,nil)
                case .failure(let error):
                    print(error)
                    completion(nil,error)
            }
        })
    }
}
