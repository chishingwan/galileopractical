//
//  HomePageViewController.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire
import SVGKit

class HomePageViewController: UIViewController {

    let disposeBag = DisposeBag()
    var countryViewModel = CountriesViewModel()
    
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryViewModel.fetchCountryData()
        displayCountries()

    }
    
    func displayCountries(){
        // data binding
        let countryItems = countryViewModel.countryList
        countryItems.bind(to: tableView.rx.items(cellIdentifier: Constant.cell, cellType: CountryCells.self)){ index, country, cell in
            cell.cName.text = country.countryName
            let url = URL(string: country.flag!)!
            let svgData: SVGKImage = SVGKImage(contentsOf: url)
            cell.flag.image = svgData.uiImage
        }.disposed(by: disposeBag)
        
        // select country
        tableView.rx.itemSelected
        .subscribe(onNext: { [weak self] indexPath in
            self!.tableView.deselectRow(at: indexPath, animated: false)
            self!.performSegue(withIdentifier: Segue.country_details.rawValue, sender: nil)
        }).disposed(by: disposeBag)
    }
    
//    func bindViewModel(){
//        let countryItems = countryViewModel.countryList
//        let query = searchBar.rx.text
//            .orEmpty
//            .distinctUntilChanged()
//
//        Observable.combineLatest(countryItems, query) { [unowned self] (countryList, query) -> [Country] in
//                return self.filteredCountries(with: countryList, query: query)
//            }
//            .bind(to: tableView.rx.items(dataSource: dataSource))
//            .disposed(by: disposeBag)
//    }
//
//    func filteredCountries(with countryList: [Country], query: String) -> [Country] {
//        guard !query.isEmpty else { return countryList }
//
//        var filteredCountries: [Country] = []
//        for section in countryList {
//            let filteredItems = section.countryName!.filter { $0.countryName.hasPrefix(query)}
//            if !filteredItems.isEmpty {
//                filteredCountries.append(Country(header: section.header, items: filteredItems))
//            }
//        }
//        return filteredCountries
//    }
}
