//
//  CountryDetailsViewController.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/14/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVGKit

class CountryDetailsViewController: UIViewController {
    
    @IBOutlet var countryName: UILabel!
    @IBOutlet var alphaCode: UILabel!
    @IBOutlet var population: UILabel!
    @IBOutlet var capital: UILabel!
    @IBOutlet var flag: UIImageView!
    
    let disposeBag = DisposeBag()
    var countryViewModel = CountriesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryViewModel.fetchCountryData()
        self.setupBindings()
        // Do any additional setup after loading the view.
    }
    
    private func setupBindings(){
        countryViewModel.alphaCode.bind(to: alphaCode.rx.text).disposed(by: disposeBag)
        countryViewModel.countryName.bind(to: countryName.rx.text).disposed(by: disposeBag)
        countryViewModel.population.bind(to: population.rx.text).disposed(by: disposeBag)
        countryViewModel.capital.bind(to: capital.rx.text).disposed(by: disposeBag)
        
        
//        let url = URL(string: )
//        let svgData: SVGKImage = SVGKImage(contentsOf: url)
//        
    }
}
