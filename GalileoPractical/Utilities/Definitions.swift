//
//  Definitions.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import Foundation
import RxSwift
import LocalAuthentication
import RxSwift
import RxCocoa
import ObjectMapper

let apiManager = APIManager()
let Singleton = SingletonData.sharedInstance
let emptyMap = Map(mappingType: MappingType.fromJSON, JSON: [:])

class APIDefinitions {
    static let urlBase = "https://restcountries.eu/rest/v2/all"
}

enum defaultValue : String {
    case empty = "no data"
}
enum Segue:String {
    case country_details = "segue_countrydetails"
}

class Constant{
    static let cell = "countryCell"
}
