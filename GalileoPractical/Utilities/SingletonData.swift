//
//  SingletonData.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import Foundation
import ObjectMapper

class SingletonData{

    static let sharedInstance = SingletonData()
    var country: Country?
    var countries: [Country]?
    var selectedCountry: Int = 0

    private init(){}
    
    func fetchCountryData(completion : @escaping (Error?)->Void) {
        if (Singleton.countries == nil){
            apiManager.getCountryData(completion: {data, error in
                if (error == nil && data != nil){
                    Singleton.countries = Mapper<Country>().mapArray(JSONString: data!)
                    completion(nil)
                } else {
                    completion(error)
                }
            })
        }
    }
}

