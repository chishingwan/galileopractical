//
//  CountriesModel.swift
//  GalileoPractical
//
//  Created by Chi Shing Wan on 1/13/20.
//  Copyright © 2020 Chi Shing Wan. All rights reserved.
//

import ObjectMapper

class Country: Mappable {
    var countryName: String?
    var flag: String?
    var capital: String?
    var alpha2Code: String?
    var population: String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        countryName <- map["name"]
        flag <- map["flag"]
        capital <- map["capital"]
        alpha2Code <- map["alpha2code"]
        population <- map["population"]
    }
}

